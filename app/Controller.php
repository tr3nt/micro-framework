<?php

namespace app;

use app\lib\Mailer;
use app\lib\Helpers;
use app\lib\AuthService;
use app\lib\external\Request;
use app\lib\external\Router;


class Controller extends Helpers
{
    public function init()
    {
        /** Set router */
        $router = new Router(new Request);

        /** Routes */
        $router->get('/', function () {
            $this->view('index');
        });
    }

    public function __construct()
    {
        /** Init MySQL connection */
        // $this->connect();
        /** Init Authentication service */
        $this->auth = new AuthService($this);
    }
    public function __destruct()
    {
        /** Close MySQL connection */
        $this->closeConnection();
    }
}
