<?php

namespace app\lib;


abstract class Helpers extends DataBase
{
    /**
     * Set view based on HTML files
     * 
     * @param mixed $title without .html extension or array with alternative layout
     * @param array pass params to view
     */
    protected function view($title, array $params = [])
    {
        $v = is_array($title) ? new View($title[0], $title[1]) : new View($title);
        $v->params(['auth' => $this->auth->auth()]);
        if (count($params) > 0)
            $v->params($params);
        $v->render();
    }

    /**
     * Redirect to another Url
     * @param string $url
     */
    public function redirect(string $url = '')
    {
        header('Location: ' . MAIN_URL . "/{$url}");
        exit();
    }

    /**
     * POST data sent by form
     */
    public function post(string $title)
    {
        if (isset($_POST[$title]))
            return $_POST[$title];
        return false;
    }

    /**
     * Convert to JSON UTF-8 and return
     *
     * @param mixed $data
     * @return string
     */
    public function json($data): string
    {
        return json_encode($data, JSON_UNESCAPED_UNICODE);
    }

    /**
     * Convert to JSON UTF-8 and print
     *
     * @param mixed $data
     */
    public function response($data)
    {
        echo $this->json($data);
        exit;
    }

    /**
     * Set flash message on page refresh
     *
     * @param string $message flash
     */
    public function flash(string $message)
    {
        $_SESSION['flash'] = $message;
    }

    /**
     * REST SERVICE PROTECTION
     */
    protected function protect(): bool
    {
        if ($this->post('key') !== $_SESSION['key']) {
            $this->result = [
                'error' => true,
                 'data' => 'You don\'t have permissions.'
            ];
            return false;
        }
        return true;
    }
}
