<?php

namespace app\lib;

use PDO;
use Exception;
use PDOException;

/**
 * Database Connection for PHP7 and MySQL
 *
 * @author Ezhaym N
 */
abstract class DataBase
{
    public $result;
    protected $auth;
    private $conn;

    /**
     * Create new PDO Object
     */
    protected function connect()
    {
        $user = DB_USER;
        $pass = DB_PASS;
        $base = DB_NAME;
        $host = DB_HOST;

        $this->conn = new PDO("mysql:host={$host};dbname={$base};charset=utf8mb4", $user, $pass,
                         [PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8mb4 COLLATE utf8mb4_unicode_ci']);
        $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    /**
     * Execute SQL to get Success or Error message
     *
     * @param string $sql query to execute
     * @param array $params parameters to search (optional)
     * @param bool $close MySQL connection
     */
    public function execute(string $sql, array $params = [])
    {
        $res = [false, 'Successful.'];
        $query = $this->conn->prepare($sql);

        try {
            $query->execute($params);
        } catch (PDOException $e) {
            $res = [true, $e->getMessage()];
        } catch (Exception $e) {
            $res = [true, $e->getMessage()];
        } finally {
            $this->result = [
                'error' => $res[0],
                'data' => $res[1]
            ];
        }
    }

    /**
     * Execute SQL to get data by SELECT
     *
     * @param string $sql SQL to execute
     * @param array $params parameters to search (optional)
     * @param bool $close MySQL connection
     * 
     */
    public function executeGet(string $sql, array $params = [])
    {
        $res = [true, "Data not found"];
        $query = $this->conn->prepare($sql);

        try {
            $query->execute($params);
            $data = $query->fetchAll(PDO::FETCH_ASSOC);
            if (count($data) > 0)
                $res = [false, $data];
        } catch (PDOException $e) {
            $res = [true, $e->getMessage()];
        } catch (Exception $e) {
            $res = [true, $e->getMessage()];
        } finally {
            $this->result = [
                'error' => $res[0],
                'data' => $res[1]
            ];
        }
    }

    /**
     * Add success message, do nothing if error
     * 
     * @param string $message
     */
    public function setMsj(string $msj)
    {
        if (!$this->result['error'])
            $this->result['data'] = $msj;
    }

    /**
     * Close MySQL connection
     */
    protected function closeConnection()
    {
        $this->conn = null;
    }
}
