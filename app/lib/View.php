<?php

namespace app\lib;

/**
 * Generar vistas a partir del controlador y un archivo HTML
 *
 * @author Ezhaym N
 */
class View
{
    private $html;
    private $layout;
    private $params;

    /**
     * Set new view with html title and layout title (optional)
     */
    public function __construct(string $title, string $layout = 'main')
    {
        $this->params = [
             'base' => MAIN_URL,
            'title' => TITLE,
            'flash' => '',
              'key' => ''
        ];
        $this->html = file_get_contents(VIEW_URL . "{$title}.html");
        $this->layout = file_get_contents(VIEW_URL . "layout/{$layout}.html");
        $this->setKey();
        $this->flash();
    }

    /**
     * Add variables to view before render
     *
     * @param array $data array con las variables a mostrar
     */
    public function params(array $data)
    {
        $this->params = array_merge($this->params, $data);
    }

    /**
     * Render view on screen
     */
    public function render()
    {
        // Se carga el html en el Layout
        $this->layout  = str_replace("{ content }", $this->html, $this->layout);
        // Se cargan los data al layout
        foreach ($this->params as $k => $i) {
            $this->layout = str_replace("{ {$k} }", $i, $this->layout);
        }
        echo $this->layout;
        exit;
    }

    /**
     * Set Flash Message (if exist)
     */
    private function flash()
    {
        if (isset($_SESSION['flash'])) {
            $this->params['flash'] = $_SESSION['flash'];
            unset($_SESSION['flash']);
        }
    }

    /**
     * Clave de seguridad
     */
    private function setKey()
    {
        $key = random_bytes(10);
        $_SESSION['key'] = bin2hex($key);
        $this->params['key'] = $_SESSION['key'];
    }
}