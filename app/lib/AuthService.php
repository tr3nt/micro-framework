<?php

namespace app\lib;

use app\Controller;


class AuthService
{
    private $con;

    /**
     * Obtain Helpers and MySQL connection
     * 
     * @param Controller $con
     */
    public function __construct(Controller $con)
    {
        $this->con = $con;
    }

    /**
     * Check if user is logged and authenticated
     * 
     * @return bool
     */
    public function auth(): bool
    {
        if (isset($_SESSION['user']) && isset($_SESSION['pass']))
            return $this->checkUser($_SESSION['user'], $_SESSION['pass']);
        return false;
    }

    /**
     * Check if user exists and create session with user and password
     * 
     * @param string $redirectTo redirect after login
     */
    public function login(array $params, string $redirectTo = '')
    {
        $_SESSION['user'] = $params['username'];
        $_SESSION['pass'] = $params['password'];

        if (!$this->checkUser($_SESSION['user'], $_SESSION['pass']))
            $this->logout('Invalid user and/or password.', 'danger');
        else
            $this->con->redirect($redirectTo);
    }

    /**
     * Destroy user and password session and redirects to login view
     * 
     * @param string $message could be an error message
     * @param string $alertType color of alert message (Bootstrap 4)
     */
    public function logout(string $message = 'Logged out.', string $alertType = 'info')
    {
        unset($_SESSION['user']);
        unset($_SESSION['pass']);

        $this->con->flash("<div class='alert alert-{$alertType}' role='alert'>{$message}</div>");
        $this->con->redirect('login');
    }

    /**
     * Search in Database if user exists and password is correct
     * 
     * @param string $user
     * @param string $pass
     */
    private function checkUser(string $user, string $pass): bool
    {
        $tableName = DB_USER_TABLE_NAME;
        $tableUser = DB_USER_TABLE_USER;
        $tablePass = DB_USER_TABLE_PASS;
        $sql = <<<SQL
            SELECT COUNT(*) AS c
              FROM {$tableName}
             WHERE {$tableUser} = :user
               AND {$tablePass} = :pass
        SQL;
        $this->con->executeGet($sql, ['user' => $user, 'pass' => sha1($pass)]);
        return !$this->con->result['error'] ? (int)$this->con->result['data'][0]['c'] > 0 : false;
    }
}
