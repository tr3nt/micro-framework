<?php

namespace app\lib;

use app\lib\external\PHPMailer;

/**
 * Send emails with PHPMailer class.
 * Data obtained from /config.inc.php
 */
final class Mailer
{
    /**
     * Send email with predefined Message.
     *
     * @param string $message Message (can be HTML format)
     * @return array
     */
    public static function sendMail(string $message): array
    {
        /** new PHPMailer instance */
        $mail = new PHPMailer();
        $mail->CharSet  = "UTF-8";
        $mail->IsSMTP();
        $mail->Host     = MAIL_HOST;
        $mail->From     = MAIL_USER;
        $mail->FromName = MAIL_FROM_NAME;
        $mail->Subject  = MAIL_TITLE;
        $mail->MsgHTML($message);
        $mail->AddAddress(MAIL_TO_EMAIL, MAIL_TO_NAME);
        $mail->SMTPAuth = true;
        $mail->Username = MAIL_USER;
        $mail->Password = MAIL_PASS;

        $success = [
            'error' => false,
            'message' => 'Email sent successfully.'
        ];
        $fail = [
            'error' => true,
            'message' => 'Email failed. Try again.'
        ];

        /** Production: SEND_MAIL must be TRUE in /config.inc.php */
        if (!SEND_MAIL)
            return $success;
        return $mail->Send() ? $success : $fail;
    }
}
