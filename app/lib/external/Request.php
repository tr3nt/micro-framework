<?php

namespace app\lib\external;

/**
 * Credits: John O. Paul
 * https://medium.com/@johnopaul
 */

class Request implements IRequest
{
  public string $requestMethod;
  public string $requestUri;
  public string $serverProtocol;

  function __construct()
  {
    $this->requestMethod = $_SERVER['REQUEST_METHOD'];
    $this->requestUri = $_SERVER['REQUEST_URI'];
    $this->serverProtocol = $_SERVER['SERVER_PROTOCOL'];
  }

  public function getBody()
  {
    if($this->requestMethod === "GET")
    {
      return;
    }


    if ($this->requestMethod == "POST")
    {

      $body = array();
      foreach($_POST as $key => $value)
      {
        $body[$key] = filter_input(INPUT_POST, $key, FILTER_SANITIZE_SPECIAL_CHARS);
      }

      return $body;
    }
  }
}
