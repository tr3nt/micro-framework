<?php

namespace app\lib\external;

/**
 * Credits: John O. Paul
 * https://medium.com/@johnopaul
 */

class Router
{
  private $request;
  private $supportedHttpMethods = array(
    "GET",
    "POST"
  );
  private array $get;
  private array $post;

  function __construct(IRequest $request)
  {
   $this->request = $request;
  }

  function __call($name, $args)
  {
    list($route, $method) = $args;

    if(!in_array(strtoupper($name), $this->supportedHttpMethods))
      $this->invalidMethodHandler();

    $b = strlen(stripslashes(BASE)) > 0 ? '/'.stripslashes(BASE) : BASE;

    if ($name === 'get')
      $this->get[$this->formatRoute($b . $route)] = $method;
    else
      $this->post[$this->formatRoute($b . $route)] = $method; 
  }

  /**
   * Removes trailing forward slashes from the right of the route.
   * @param route (string)
   */
  private function formatRoute($route)
  {
    $result = rtrim($route, '/');
    if ($result === '')
    {
      return '/';
    }
    return $result;
  }

  private function invalidMethodHandler()
  {
    header("{$this->request->serverProtocol} 405 Method Not Allowed");
  }

  private function defaultRequestHandler()
  {
    header("{$this->request->serverProtocol} 404 Not Found");
  }

  /**
   * Resolves a route
   */
  function resolve()
  {
    $methodDictionary = $this->{strtolower($this->request->requestMethod)};
    $formatedRoute = $this->formatRoute($this->request->requestUri);
    $method = $methodDictionary[$formatedRoute];

    if(is_null($method))
    {
      $this->defaultRequestHandler();
      return;
    }

    echo call_user_func_array($method, array($this->request));
  }

  function __destruct()
  {
    $this->resolve();
  }
}
