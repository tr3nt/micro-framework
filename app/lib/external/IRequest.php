<?php

namespace app\lib\external;

/**
 * Credits: John O. Paul
 * https://medium.com/@johnopaul
 */

interface IRequest
{
    public function getBody();
}
