<?php

/** Absolute route on hard disk */
define('MAIN_DIR', dirname(dirname(__FILE__)) . '/');

/** Load Configuration file */
if (!file_exists(MAIN_DIR . 'config.inc.php')) {
    echo 'Please create a config.inc.php file';
    exit;
}
require_once MAIN_DIR . 'config.inc.php';


/** Init App */
$app = new app\Controller;
$app->init();
