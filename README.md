# Micro Framework v2.1

Built with PHP7, Bootstrap 4 and jQuery.

A. Create _/config.inc.php_ file, based on _/config.inc.example.php_ file... Add your Site and SMTP mailer data config.

## Views

B. Edit _/app/views/layout/main.html_ as your main template

C. Example: Create view called About example: _/app/views/about.html_

```html
<div class="row">
    <div class="col col-12">
        <h1 class="text-center">About</h1>
    </div>
</div>
```

D. Example: create About section with Router in _/app/Controller.php_ with html file name

```php
$router->get('/about', function () {
    $this->view('about');
});
```
E For alternative template use an array with both names:

```php
$router->get('/about', function () {
    $this->view(['about', 'alter_layout']);
});
```

F. Url must target to **/about** to work, example: _host.com/about_

F.1 Apache users, make sure to enable Rewrite Mod

F.2 Nginx users, change configuration to target all requests to index.php

## Variables.

A.1. Controller/View variable example:

```php
$router->get('/about', function () {
    $params = ['name' => 'John Doe']; // pass dynamic values to view
    $this->view('about', $params);
});
```

A.2. Process in __about.html__ view

```html
<h1>Hello { name }</h1>
```

## DataBase

A.1. REST Select service example ( executeGet() method )
```php
$router->post('/get-people', function () {
    $this->executeGet('SELECT * FROM people');
    $this->response($this->result); // Convert results to JSON and print
});
```

A.2. jQuery AJAX call example
```html
<ul id="ppl"></ul>

<script>
$.post(
    '/get-people',
    people => {
        if (people.error)
            alert(people.data); // If Error, data will be string Error Message
        else
            people.data.map(person => {
                $('#ppl').append(`<li>${person.name}</li>`)
            })
    },
    'json'
);
</script>
```

A.3. REST Create, Update, Delete example ( execute() method )
```php
$router->post('/create-people', function ($request) {
    $post = $request-getBody(); // Obtain $_POST data
    $params = [
        'nam' => $post['name'],
        'ema' => $post['email']
    ];
    $this->execute('INSERT INTO people (name, email) VALUES (:nam, :ema)', $params);
    $this->response($this->result); // Convert results to JSON and print
});
```

## Authentication

A.1. Login html example (with validation)
```html
<form id="form" action="/login" method="POST">
    <div class="row justify-content-center">
        <div class="col-6 form-group">
            <label for="user">Email</label>
            <input name="user" id="user" type="email" class="form-control">
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-6 form-group">
            <label for="pass">Password</label>
            <input name="pass" id="pass" type="password" class="form-control">
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-6 form-group">
            { flash } <!-- Flash error Alert or success Alert -->
            <button type="submit" class="btn btn-primary btn-lg">
                Login
            </button>
        </div>
    </div>
</form>

<script>
$('#form').validate({
    rules: {
        user: {
            required: true,
            email: true
        },
        pass: {
            required: true
        }
    }
})
</script>
```

A.2. Login PHP example
```php
$route->post('/login', function ($request) {
    /**
     * It will search for user and pass POST variables,
     * log and redirect to 'index'. Or 'login' if fails
     */
    $this->auth->login($request->getBody());
});
```

A.3. Protected view example
```php
$route->get('/home', function () {
    $this->view($this->auth->auth() ? 'home' : 'login');
});
```

## Mailer

A.1. Contact form html example (with validation)
```html
<form id="form" action="/send-mail" method="POST">
    <div class="row justify-content-center">
        <div class="col-6 form-group">
            <label for="name">Name</label>
            <input name="name" id="name" class="form-control">
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-6 form-group">
            <label for="email">Email</label>
            <input name="email" id="email" type="email" class="form-control">
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-6 form-group">
            <label for="comments">Comments</label>
            <textarea class="form-control" name="comments" id="comments" rows="4"></textarea>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-6 form-group">
            { flash } <!-- Flash message -->
            <button type="submit" class="btn btn-primary btn-lg">
                Send
            </button>
        </div>
    </div>
</form>

<script>
$('#form').validate({
    rules: {
        name: {
            required: true
        },
        email: {
            required: true,
            email: true
        },
        comments: {
            required: true
        }
    }
})
</script>
```

Contact form PHP example:
```php
$route->post('send-mail', function ($request) => {
    $post = $request->getBody();
    $message = "name: {$post['name']} <br> email: {$post['email']} <br> comments: {$post['comments']}";
    Mailer::sendMail($message, 'contact'); // Automatic set alert (error or success) and redirects to contact view
});
```

## Helpers

1. Redirect
```php
$this->redirect('contact');
```

2. Flash message (set before view or redirect)
```php
$this->flash('Welcome to my website');
```

3. $_POST data sent by Form (inside Router method)
```php
$post = $request->getBody();
echo $post['name'];
```

4. JSON in UTF-8 unicode
```php
$this->json($value);
```

5. Response in JSON (for REST)
```php
$this->response($value);
```
