<?php

/** Show errors, comment or delete on Production */
ini_set('display_errors', 'On');
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


/** Init session */
session_start();


/** Autoloader */
spl_autoload_register(function ($className) {
    include MAIN_DIR . str_replace('\\', '/', $className) . ".php";
});


/** Site Constants */
define('TITLE', "Micro Framework");
define('MAIN_URL', "http://localhost");
define('VIEW_URL', MAIN_DIR . 'app/views/');

// BASE determine if project is on a subfolder, example: 'blog'
define('BASE', '');


/** Database Constants */
define('DB_HOST', 'localhost');
define('DB_NAME', 'test');
define('DB_USER', 'homestead');
define('DB_PASS', 'secret');


/** Mail Server data */
define("SEND_MAIL",  false); // Set to 'true' to send real mails (production)

define("MAIL_FROM_NAME", "Website of Company Inc");
define("MAIL_TO_NAME",   "Company Inc");
define("MAIL_TO_EMAIL",  "contact@company.com");

define("MAIL_TITLE", "New mail from Company Inc webpage");

define("MAIL_USER", "mailer@company.com");
define("MAIL_PASS", "12345678");
define("MAIL_HOST", "mail.company.com");
